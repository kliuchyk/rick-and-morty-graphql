/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';

import * as Types from '../types/types.graphql';
const defaultOptions = {};
export type CharactersQueryVariables = Types.Exact<{
  page?: Types.Maybe<Types.Scalars['Int']>;
  filter?: Types.Maybe<Types.FilterCharacter>;
}>;

export type CharactersQuery = {
  __typename?: 'Query';
  characters?:
    | {
        __typename?: 'Characters';
        info?:
          | {
              __typename?: 'Info';
              count?: number | null | undefined;
              pages?: number | null | undefined;
              next?: number | null | undefined;
              prev?: number | null | undefined;
            }
          | null
          | undefined;
        results?:
          | Array<
              | {
                  __typename?: 'Character';
                  id?: string | null | undefined;
                  name?: string | null | undefined;
                  image?: string | null | undefined;
                }
              | null
              | undefined
            >
          | null
          | undefined;
      }
    | null
    | undefined;
};

export const CharactersDocument = gql`
  query Characters($page: Int, $filter: FilterCharacter) {
    characters(page: $page, filter: $filter) {
      info {
        count
        pages
        next
        prev
      }
      results {
        id
        name
        image
      }
    }
  }
`;

/**
 * __useCharactersQuery__
 *
 * To run a query within a React component, call `useCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `useCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCharactersQuery({
 *   variables: {
 *      page: // value for 'page'
 *      filter: // value for 'filter'
 *   },
 * });
 */
export function useCharactersQuery(
  baseOptions?: Apollo.QueryHookOptions<CharactersQuery, CharactersQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<CharactersQuery, CharactersQueryVariables>(CharactersDocument, options);
}
export function useCharactersLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<CharactersQuery, CharactersQueryVariables>,
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<CharactersQuery, CharactersQueryVariables>(
    CharactersDocument,
    options,
  );
}
export type CharactersQueryHookResult = ReturnType<typeof useCharactersQuery>;
export type CharactersLazyQueryHookResult = ReturnType<typeof useCharactersLazyQuery>;
export type CharactersQueryResult = Apollo.QueryResult<CharactersQuery, CharactersQueryVariables>;
