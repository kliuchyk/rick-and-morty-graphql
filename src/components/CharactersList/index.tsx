import { gql } from '@apollo/client';
import { ReactElement, useEffect } from 'react';

import { useCharactersLazyQuery } from '../../api';

const GET_CHARACTERS = gql`
  {
    characters(page: 2) {
      results {
        name
        id
        image
      }
    }
  }
`;

export function CharactersList(): ReactElement {
  const [fetchCharacters, { loading, error, data }] = useCharactersLazyQuery();

  useEffect(() => {
    fetchCharacters({
      variables: {
        page: 1,
      },
    });
  }, [fetchCharacters]);

  return (
    <div>
      <h1>Characters List</h1>
    </div>
  );
}
