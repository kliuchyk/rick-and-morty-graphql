import { ReactElement, ReactNode, useEffect, useRef, useState } from 'react';

import { NavMenu } from '../NavMenu';

// const PageLayoutW = styled.div`
//   display: flex;
// `;

// interface MainProps {
//   marginLeft: number;
// }

// const Main = styled.main<MainProps>`
//   width: 100%;
//   margin-left: ${({ marginLeft }) => `${marginLeft}px`};
//   overflow-x: hidden;
// `;

export interface PageLayoutProps {
  children: ReactNode;
}

export function PageLayout({ children }: PageLayoutProps): ReactElement {
  return (
    <div>
      <NavMenu />
      <div data-testid="main-content">{children}</div>
    </div>
  );
}
