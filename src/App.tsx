import { ApolloProvider } from '@apollo/client';
import { ReactElement } from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';

import { client } from './api';
import { CharactersList, PageLayout } from './components';

function App(): ReactElement {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <PageLayout>
          <div className="App">Rick N Motry</div>
          <Switch>
            <Redirect exact path="/" to="/characters" />
            {/* <Route exact path="/characters" component={CharactersList} /> */}
            <Route exact path="/characters" component={CharactersList} />
          </Switch>
        </PageLayout>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
